#include "polynom.h"
#include <stdlib.h>
#include <iostream>

using namespace std;

int Fraction::counter = 0;

void Fraction::reduction() {
    int temp = 1;
    int least = (numerator < denominator)?numerator:denominator;
    for (int i = 1; i <= abs(least); i++) {
        if ((numerator%i == 0) && (denominator%i == 0)) {
            temp = i;
        }
    }
    if (temp!=1) {
        numerator/=temp;
        denominator/=temp;
    }
}

Fraction::Fraction(int up, int down) {
    if (down == 0 || up == 0) {
        throw "Not"; //myException(1, number)
    } else if (down < 0 && up < 0) {
        numerator = abs(up);
        denominator = abs(down);
    } else {
        numerator = up;
        denominator = down;
        intpart = 0;
    }
    //Сокращение дроби
    reduction();
    //Конец сокращения
}

Fraction::Fraction(const Fraction& copy) {
    numerator = copy.numerator;
    denominator = copy.denominator;
    intpart = copy.intpart;
}

int Fraction::getNumerator() const {
    return numerator;
}

int Fraction::getDenominator() const {
    return denominator;
}

int Fraction::getIntpart() const {
    return intpart;
}

void Fraction::conversionToRight() {
    if (abs(numerator) > abs(denominator)) {
        intpart = abs(numerator)/abs(denominator);
        numerator %= denominator;
    } else {
        cout << "Fraction is right" << endl;
    }
}

void Fraction::conversionToWrong() {
    if (abs(numerator) < abs(denominator) && intpart > 0) {
        numerator += intpart*denominator;
        intpart = 0;
    } else {
        cout << "Fraction is wrong" << endl;
    }
}

Fraction& Fraction::operator+=(const Fraction& rhs) {
    numerator = (intpart*denominator+numerator)*rhs.denominator + (rhs.intpart*rhs.denominator+rhs.numerator)*denominator;
    denominator *= rhs.denominator;
    if (intpart != 0 && (abs(numerator) > abs(denominator))) {
        reduction();
        conversionToRight();
    } else {
        intpart = 0;
        reduction();
    }
    return *this;
}

Fraction& Fraction::operator+=(int value) {
    numerator = value*denominator+(intpart*denominator+numerator);
    if (intpart != 0 && (abs(numerator) > abs(denominator))) {
        reduction();
        conversionToRight();
    } else {
        intpart = 0;
        reduction();
    }
    return *this;
}

Fraction& Fraction::operator-=(const Fraction& rhs) {
    numerator = (intpart*denominator+numerator)*rhs.denominator - (rhs.intpart*rhs.denominator+rhs.numerator)*denominator;
    denominator *= rhs.denominator;
    if (intpart != 0 && (abs(numerator) > abs(denominator))) {
        reduction();
        conversionToRight();
    } else {
        intpart = 0;
        reduction();
    }
    return *this;
}

Fraction& Fraction::operator-=(int value) {
    numerator = intpart*denominator+numerator - value*denominator;
    if (intpart != 0 && (abs(numerator) > abs(denominator))) {
        reduction();
        conversionToRight();
    } else {
        intpart = 0;
        reduction();
    }
    return *this;
}

Fraction& Fraction::operator*=(const Fraction& rhs) {
    numerator = (intpart*denominator+numerator) * (rhs.intpart*rhs.denominator+rhs.numerator);
    denominator *= rhs.denominator;
    if (intpart != 0 && (abs(numerator) > abs(denominator))) {
        reduction();
        conversionToRight();
    } else {
        intpart = 0;
        reduction();
    }
    return *this;
}

Fraction& Fraction::operator*=(int value) {
    //ПОДУМАТЬ: Как обрабатывать нуль
    numerator = (intpart*denominator+numerator) * value;
    if (intpart != 0 && (abs(numerator) > abs(denominator))) {
        reduction();
        conversionToRight();
    } else {
        intpart = 0;
        reduction();
    }
    return *this;
}

Fraction& Fraction::operator/=(const Fraction& rhs) {
    /*if (rhs.denominator == 0) {
        throw "Not";
    }*/
    numerator = (intpart*denominator+numerator) * rhs.denominator;
    denominator *= (rhs.intpart*rhs.denominator+rhs.numerator);
    if (intpart != 0 && (abs(numerator) > abs(denominator))) {
        reduction();
        conversionToRight();
    } else {
        intpart = 0;
        reduction();
    }
    return *this;
}

Fraction& Fraction::operator/=(int value) {
    if (value == 0) {
        throw "Not";
    }
    denominator *= value;
    if (intpart !=0 && (abs(numerator) > abs(denominator))) {
        reduction();
        conversionToRight();
    } else {
        intpart = 0;
        reduction();
    }
    return *this;
}

ostream& operator<<(ostream& os, const Fraction& out) {
    if (out.intpart == 0) {
        os << out.numerator << "/" << out.denominator << endl;
    } else {
        os << out.intpart << "(" << out.numerator << "/" << out.denominator << ")" << endl;
    }
    return os;
}

Fraction operator+(const Fraction& lhs, const Fraction& rhs) {
    Fraction result(lhs);
    result+=rhs;
    return result;
}

Fraction operator+(const Fraction& lhs, int value) {
    Fraction result(lhs);
    result+=value;
    return result;
}

Fraction operator+(int value, const Fraction& rhs) {
    Fraction result(rhs);
    result+=value;
    return result;
}

Fraction operator-(const Fraction& lhs, const Fraction& rhs) {
    Fraction result(lhs);
    result-=rhs;
    return result;
}

Fraction operator-(const Fraction& lhs, int value) {
    Fraction result(lhs);
    result-=value;
    return result;
}

Fraction operator*(const Fraction& lhs, const Fraction& rhs) {
    Fraction result(lhs);
    result*=rhs;
    return result;
}

Fraction operator*(const Fraction& lhs, int value) {
    Fraction result(lhs);
    result*=value;
    return result;
}

Fraction operator*(int value, const Fraction& rhs) {
    Fraction result(rhs);
    result*=value;
    return result;
}

Fraction operator/(const Fraction& lhs, const Fraction& rhs) {
    Fraction result(lhs);
    result/=rhs;
    return result;
}

Fraction operator/(const Fraction& lhs, int value) {
    Fraction result(lhs);
    result/=value;
    return result;
}

Fraction operator/(int value, const Fraction& rhs) {
    Fraction result(rhs);
    result/=value;
    return result;
}
