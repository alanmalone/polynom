#include <iostream>
#include "polynom.h"

using namespace std;

int main() {
    Fraction A(10, 9);

    A.conversionToRight();
    cout << A << endl;
    Fraction B(1, 2);
    Fraction C;
    C = 2 / B;
    cout << C;
}
