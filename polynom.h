#ifndef POLYNOM_H_INCLUDED
#define POLYNOM_H_INCLUDED

#include <iostream>

using namespace std;

class Fraction {
private:
    static int counter;
    int number;
    int numerator; //числитель
    int denominator; //знаменатель
    int intpart; //целая часть
    void reduction(); //сокращение дроби
public:
    Fraction(int = 1, int = 1);
    Fraction(const Fraction&);
    int getNumerator() const;
    int getDenominator() const;
    int getIntpart() const;
    void conversionToRight(); //выделение целой части
    void conversionToWrong(); //Преобразование в неправильную
    Fraction& operator+=(const Fraction&);
    Fraction& operator+=(int);
    Fraction& operator-=(const Fraction&);
    Fraction& operator-=(int);
    Fraction& operator*=(const Fraction&);
    Fraction& operator*=(int);
    Fraction& operator/=(const Fraction&);
    Fraction& operator/=(int);
    friend ostream& operator<<(ostream &os, const Fraction&);
};

Fraction operator+(const Fraction&, const Fraction&);
Fraction operator+(const Fraction&, int);
Fraction operator+(int, const Fraction&);
Fraction operator-(const Fraction&, const Fraction&);
Fraction operator-(const Fraction&, int);
Fraction operator*(const Fraction&, const Fraction&);
Fraction operator*(const Fraction&, int);
Fraction operator*(int, const Fraction&);
Fraction operator/(const Fraction&, const Fraction&);
Fraction operator/(const Fraction&, int);
Fraction operator/(int, const Fraction&);

#endif // POLYNOM_H_INCLUDED
